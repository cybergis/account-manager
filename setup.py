import os
from setuptools import setup

# Utility function to read the README file.
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup (
    name = "Account Manager",
    version = "1.0.0",

    install_requires = ['requests'],
    packages = ['acctmgr', 'tests'],
    scripts = ['scripts/acctmgr'],

    author = "Garrett Nickel",
    author_email = "gmnicke2@illinois.edu",
    description = ("Extended linux login creation utility"),
    license = "NCSA Open Source",
    keywords = "accounts login",
    url = "http://bitbucket.org/cybergis/account_manager",
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 2 - Beta",
        "Topic :: Utilities",
    ],
)
