#!/usr/bin/env python

###########################################################
# Copyright (c) 2015 CyberGIS Center for Advanced Digital #
# and Spatial Studies (CyberGIS). All Rights Reserved     #
###########################################################
# acctmgr: command-line utility for automatic account setup
# Author: Garrett Nickel <gmnicke2@illinois.edu>
# Date: 08/20/2015

import json
import requests

endpoint = ""

def set_endpoint (target_endpoint) : 
    global endpoint
    endpoint = target_endpoint

def rest(resource, method,  **message) :
    """Calls the REST endpoint passing any necessary keyword arguments
        'rest' is a basic wrapper around the HTTP request to the REST endpoint

    Args:
        resource (str): the resource to query
        method (str): the HTTP method that will be called
        message (optional): keywords to optionally include

    Raises:
        Raises exceptions resulting from errors invoking the requests module
        functions
    """
    global endpoint
    url = endpoint.rstrip('/') + '/' + resource
    if(method.upper()=='POST') :
        r = requests.request(method.upper(), url, timeout=50, data=message)
        return
    else :
        r = requests.request(method.upper(), url, timeout=50, params=message)

    r.raise_for_status()

    response = r.json()

    return response


def dedup_concat(responses):
    key_set=set()
    result=json.loads('[]')
    for response in responses:
        for i in range(0, len(response)):
            key = ''.join((response[i]['user_id'],response[i]['_id']))
            if key not in key_set:
                key_set.add(key)
                result.append(response[i])
    return result


def search_user(keyword):
    if (keyword != "") :
        user_name = 'users?userName='
        user_response = rest(user_name + keyword, 'GET')
        full_name = 'users?fullName='
        full_response = rest(full_name + keyword, 'GET')
        response = dedup_concat((user_response,full_response))
        if (len(response) > 0) :
            for user_info in response :
                print(user_info['username'])
               
        else :    
            print('User "'+keyword+'" Can not be found.')
    else :
        print('Please specify keyword') 


def print_group_name():
    resource='groups'
    response = rest(resource, 'GET')
    if (len(response) > 0) :
        for group_info in response :
            print('Team name : ' + group_info['name'] + 
                 ' Description : ' + group_info['description'])
    else : 
        print('No group found')


