#!/usr/bin/env python

###########################################################
# Copyright (c) 2015 CyberGIS Center for Advanced Digital #
# and Spatial Studies (CyberGIS). All Rights Reserved     #
###########################################################
# acctmgr: command-line utility for automatic account setup
# Origin Author: Garrett Nickel <gmnicke2@illinois.edu>
# Date: 08/20/2015

import os
import sys
import time
import subprocess
import logging
from rest import *




# TODO really need to be checking return codes at each step
def execute_shell(commands) :
    """Creates and waits for subprocesses that execute shell commands involved
        with various filesystem creation steps. Prints each command as it is
        executed, as well as all non-empty stdout piped into the subprocess

    Args:
        commands (list): list of bash shell commands to iterate through

    Returns:
        None
    """

    for command in commands :
        print('Executing "'+command+'"')
        process = subprocess.Popen(command, shell=True, 
            stdout=subprocess.PIPE)
        output = process.communicate()[0]
        if(output != "") :
            logging.warning(output)
        process.wait()

def remove_account(linux_username) : 
    commands = ['userdel ' + linux_username, 'rm -rf /home/' + linux_username]
    execute_shell(commands)
    print ('User ' + linux_username + ' deleted from the system')

def authorize_key(username, public_key) :
    """Handles copying the user's SSH public key into authorized_keys

    Args:
        username (string): username of the new user
        public_key (string): spublic SSH key of the new user

    Returns:
        None
    """

    ssh_dir = '/home/'+username+'/.ssh'

    commands = [
        'mkdir '+ssh_dir,
        'chmod 700 '+ssh_dir,
        'echo "'+public_key+'" > '+ssh_dir+'/authorized_keys',
        'chmod 600 '+ssh_dir+'/authorized_keys',
        'chown -R '+username+':'+username+' '+ssh_dir
    ]
    execute_shell(commands)

def authorize_key_easy(username, public_key) :
    """Handles copying the user's SSH public key into authorized_keys

    Args:
        username (string): username of the new user
        public_key (string): spublic SSH key of the new user

    Returns:
        None
    """

    ssh_dir = '/home/'+username+'/.ssh'

    commands = [
        'mkdir '+ssh_dir,
        'chmod 700 '+ssh_dir,
        'echo "'+public_key+'" > '+ssh_dir+'/authorized_keys',
        'chmod 600 '+ssh_dir+'/authorized_keys',
        'chown -R '+username+':'+username+' '+ssh_dir
    ]
    execute_shell_easy(commands)


def handle_preferences(username, repo) :
    """Handles pulling from and copying information/files from the user's
        preference repo. This includes creating a new .vimrc from the user's
        as well as appending to .bashrc

    Args:
        username (string): username of the new user
        repo (string): code repository of the user's preference files

    Returns:
        None
    """

    user_dir = '/home/'+username
        
    commands = [
        'git clone '+repo+' '+user_dir+'/preferences',
        'mv '+user_dir+'/preferences/* '+user_dir+'/',
        'cat '+user_dir+'/preferences/.vimrc >'+user_dir+'/.vimrc',
        'cat '+user_dir+'/preferences/.bashrc >>'+user_dir+'/.bashrc',
        'chown '+ username + ':' + username + ' ' + user_dir + '/.vimrc',
        'chown '+ username + ':' + username + ' ' + user_dir + '/*',
        'rm -rf '+user_dir+'/preferences'
        ]
    execute_shell(commands)

def handle_preferences_easy(username, repo) :
    """Handles pulling from and copying information/files from the user's
        preference repo. This includes creating a new .vimrc from the user's
        as well as appending to .bashrc

    Args:
        username (string): username of the new user
        repo (string): code repository of the user's preference files

    Returns:
        None
    """

    user_dir = '/home/'+username
    commands = [
        'git clone '+repo+' '+user_dir+'/preferences' + ' > /dev/null 2>&1',
        'mv '+user_dir+'/preferences/* '+user_dir+'/',
        'cat '+user_dir+'/preferences/.vimrc >'+user_dir+'/.vimrc',
        'cat '+user_dir+'/preferences/.bashrc >>'+user_dir+'/.bashrc',
        'chown '+ username + ':' + username + ' ' + user_dir + '/.vimrc',
        'chown '+ username + ':' + username + ' ' + user_dir + '/*',
        'rm -rf '+user_dir+'/preferences'
        ]
    execute_shell_easy(commands)


def make_sudoer(username) :
    """Executed if --sudo=True on execution, handles making the user a sudoer
        and generates a random password in ~user/home/.password

    Args:
        username (string): username of the new user

    Returns:
        None
    """

    commands = [
        'adduser {} sudo'.format(username),
        'openssl rand -base64 6 | sudo -u {} tee -a /home/{}/.password'.format(
            username, username),
        'chmod 600 /home/{}/.password'.format(username),
        'passwd {}'.format(username)
    ]
    execute_shell(commands)

def make_sudoer_easy(username) :
    """Executed if --sudo=True on execution, handles making the user a sudoer
        and generates a random password in ~user/home/.password

    Args:
        username (string): username of the new user

    Returns:
        None
    """

    commands = [
        'adduser {} sudo'.format(username),
        'openssl rand -base64 6 | sudo -u {} tee -a /home/{}/.password'.format(
            username, username),
        'chmod 600 /home/{}/.password'.format(username),
        'passwd {}'.format(username)
    ]
    execute_shell_easy(commands)


def create_user(user_list, is_sudoer) :
    """Called when the action is 'adduser', calls all functions to create
        a new user after RESTfully GETting the user's information

    Args:
        endpoint (string, URL): the REST endpoint
        user_list (list): list of username to create
        is_sudoer (bool): True is user is to be sudoer, else False

    Returns:
        None
    """
    for user in user_list : 
        response = get_user_info(user)
        if(len(response) > 0) :
            user_info = response[0]
     
            if  subprocess.Popen("getent passwd " + user_info['linuxUserName'] , shell = True, stdout = subprocess.PIPE).communicate()[0] != "" :
                print('Account "'+user_info['linuxUserName']+ ' already exist')
                continue
            full_name = user_info['full_name']
            public_key = user_info['public_key']
            repo = user_info['pref_repo']
            create_account_easy(user_info['linuxUserName'], full_name)
            notify_user(user_info['username'], 'Your account is ready')
            if(is_sudoer) :
                make_sudoer(user_info['linuxUserName'])
            authorize_key_easy(user_info['linuxUserName'], public_key)
            handle_preferences_easy(user_info['linuxUserName'], repo)
        else :
            print('User "'+user+'" has not submitted a preference form.')

def get_user_info(username) : 
    resource = 'users/'+username
    user_info = rest(resource, 'GET')
    return user_info

def get_group_info(groupname) : 
    resource = 'users?group=' + groupname
    group_info = rest(resource, 'GET')
    return group_info


def create_group(group_result, is_sudoer) :
    if os.geteuid() != 0 :
         exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
    if(len(group_result) > 0) :
        for user in group_result :
            create_user([user['username']], is_sudoer)
    else :
        print 'No group with specified group name found'

def remove_user(username) : 
    user_info = get_user_info(username)
    remove_account(user_info[0]['linuxUserName'])

def notify_user(username, req_message) :
    resource = 'notify/' + username
    rest(resource, 'POST', message = req_message)

def create_user_verbose(user_list, is_sudoer) :
    """Called when the action is 'adduser', calls all functions to create
        a new user after RESTfully GETting the user's information

    Args:
        endpoint (string, URL): the REST endpoint
        user_list (list): list of username to create
        is_sudoer (bool): True is user is to be sudoer, else False

    Returns:
        None
    """
    for user in user_list : 
        response = get_user_info(user)
        if(len(response) > 0) :
            user_info = response[0]
     
            if  subprocess.Popen("getent passwd " + user_info['linuxUserName'] , shell = True, stdout = subprocess.PIPE).communicate()[0] != "" :
                print('Account "'+user_info['linuxUserName']+ ' already exist')
                continue
            full_name = user_info['full_name']
            public_key = user_info['public_key']
            repo = user_info['pref_repo']
            create_account(user_info['linuxUserName'], full_name)
            notify_user(user_info['username'], 'Your account is ready')
            if(is_sudoer) :
                make_sudoer(user_info['linuxUserName'])
            authorize_key(user_info['linuxUserName'], public_key)
            handle_preferences(user_info['linuxUserName'], repo)
        else :
            print('User "'+user+'" has not submitted a preference form.')

def create_account_easy(username, full_name) :
    """Executes the first step -- simple account creation

    Args:
        username (string): username of the new user
        full_name (string): full name of the new user

    Returns:
        None
    """
    commands = ['useradd -c "'+full_name+'" -s /bin/bash -m '+username]
    execute_shell_easy(commands)

def create_account(username, full_name) :
    """Executes the first step -- simple account creation

    Args:
        username (string): username of the new user
        full_name (string): full name of the new user

    Returns:
        None
    """
    commands = ['useradd -c "'+full_name+'" -s /bin/bash -m '+username]
    execute_shell_easy(commands)
    print('Account "'+username+'" created, home directory at ~'+username)

def execute_shell_easy(commands) :
    """Creates and waits for subprocesses that execute shell commands involved
        with various filesystem creation steps. Prints each command as it is
        executed, as well as all non-empty stdout piped into the subprocess

    Args:
        commands (list): list of bash shell commands to iterate through

    Returns:
        None
    """

    for command in commands :
        process = subprocess.Popen(command, shell=True, 
            stdout=subprocess.PIPE)
        output = process.communicate()[0]
        if(output != "") :
            logging.warning(output)
        process.wait()


