#!/usr/bin/env python

###########################################################
# Copyright (c) 2015 CyberGIS Center for Advanced Digital #
# and Spatial Studies (CyberGIS). All Rights Reserved     #
###########################################################
# acctmgr: command-line utility for automatic account setup
# Original Author: Garrett Nickel <gmnicke2@illinois.edu>
# Date: 08/20/2015

"""
Account Manager package provides a command line utility for creating
user logins with extended featrues including:

  * integrated with a user-managed account preference database
  * creates logins based on queries for user name, email, or login
  * create multiple logins based on group membership 
  * automatically installs public keys in authorized_keys
  * creates randomized local password for sudo'ing
  * optionally installs dot files from repository

"""

import os
import sys
import argparse
from rest import *

# TODO action should probably be done with subparser
def parse_args() :
    """Defines command-line positional and optional arguments a nd checks
        for valid action input if present.

    Args: none

    Returns: A (tuple) containing the following:
        args (namespace) : used to provide certain variables
        action (string) : for main to use as a switch for calls to perform
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("action", nargs='?', type=str, default='list',
        help='list (list users) / adduser (add user & preferences) / removeuser (remove user & preferences) / addgroup (add group & preferences)')
    parser.add_argument("--sudo",
        action='store_true',
        help="User is to have sudo privileges")
    parser.add_argument("-e", "--endpoint",
        default="http://141.142.168.56/",
        help="Endpoint for the RESTful query")
    parser.add_argument("-v", "--verbose", action = "store_true", help = "enter verbose mode to process")
    parser.add_argument("-s", "--server", 
                        help = "provide the link to the preferrable files")
    parser.add_argument("-l","--login", 
                        help = "provide the login name")
    parser.add_argument("-g", "--group", 
                        help = "specify the group to be added")
    parser.add_argument("-u", "--user", nargs='*',
                        help = "provide information of a user")
    parser.add_argument("--adduser", action="store_true",
                        help="add user to the system")
    parser.add_argument("--addgroup", action="store_true",
                        help="add user to the system")
    parser.add_argument("--removeuser", action="store_true",
                        help="remove user from the system")
    parser.add_argument("--notify", action="store_true", 
                        help="notify the user using specified method")
    parser.add_argument("-m", "--message", 
        default = "Your account has been created", 
        help="specify the message to notify the user")
    parser.add_argument("--search", action="store_true",
                        help="search for user")
    parser.add_argument("--listgroup", action="store_true",
                        help="list all the group name")

    args = parser.parse_args()

    if not args.endpoint :
        print('Endpoint for REST API not specified')
        sys.exit(1)

    if args.action.lower() == 'search' :
        return (args, args.action.lower())
    if args.action.lower() == 'adduser' and args.user == "" :
        args.user = raw_input("Please specify which user to add: ")

    if args.sudo and args.action.lower() == 'adduser' :
        sudoer = raw_input("User is to be sudo-er, confirm? (Y/n): ")
        if(sudoer != 'Y') :
            args.sudo = False
            print('User "'+args.user+'" will not be sudo-er.');
        else :
            print('User "'+args.user+'" will be sudo-er.');
    else :
        args.sudo = False

    return (args, args.action.lower())


def print_user_info(user_list) :
    """Prints all users who have registered with the form and pertinent info

    Args:
        endpoint (string): the REST endpoint
        username (string): username of the new user

    Returns:
        void
    """
    resource = 'users'
    if user_list is not None:
        for user in user_list :
            if (user != "") :
                response = rest(resource+"/"+user, 'GET')
                if (len(response) > 0) :
                    print('Info for user "'+response[0]['username']+'"')
                    print('Full Name: \n\t'+response[0]['full_name'])
                else :
                    print('User "'+user+'" has not submitted a form.')
                    sys.exit(1)
    else :
        response = rest(resource, 'GET')
        for user in response :
            print('Info for user "'+user['username']+'"')
            print('Full Name: \n\t'+user['full_name'])