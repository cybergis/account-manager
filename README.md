# Account Manager

The Account Manager package provides _acctmgr_, a command line utility that 
simplifies account creation and management on a Linux machine. The acctmgr
is designed to work with an account preference web service. Users may
self-manage their account information using a web service and the acctmgr
utility queries the service for account creation details.

  * creates logins with simple lookup for name, email, or login
  * create multiple logins based on group membership
  * automatically installs public keys in authorized_keys
  * optionally enable root access using sudo
  * creates randomized local password for sudo'ing
  * optionally installs dot files from a user specified repository


### Installing

Account Manager is a standard python package. The recommended installation
method is to install it within a virtualenv.

```bash
virtualenv acctmgr
source acctmgr/bin/activate
git clone https://bitbucket.org/cybergis/account_manager
cd account_manager
python setup.py install
```

After installation you can invoke the _acctmgr_ script directly without
the need to activate the virtualenv.




